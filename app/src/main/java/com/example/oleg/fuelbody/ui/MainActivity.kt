package com.example.oleg.fuelbody.ui

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log

import com.example.oleg.fuelbody.R
import com.example.oleg.fuelbody.fragments.FragmentLauncher

class MainActivity : AppCompatActivity() {
    private val TAG: String = MainActivity::class.java.simpleName
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        FragmentLauncher(supportFragmentManager).showMap()
        supportActionBar?.hide()
        Log.d(TAG, "On create ")
    }
}
