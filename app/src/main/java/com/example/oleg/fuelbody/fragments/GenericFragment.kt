package com.example.oleg.fuelbody.fragments

import android.app.ProgressDialog
import android.content.Context
import android.support.v4.app.Fragment
import android.widget.Toast
import com.example.oleg.fuelbody.ui.MainActivity

/**
 * Created by Oleg on 10.06.2017.
 */
abstract class GenericFragment : Fragment() {
    private var mActivityBridge: MainActivity? = null
    private var progressDialog: ProgressDialog? = null

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        mActivityBridge = activity as MainActivity
    }

    protected fun showShortToast(contentString: String) {
        Toast.makeText(activity, contentString, Toast.LENGTH_SHORT).show()
    }

    protected fun showProgressDialog() {
        if (progressDialog == null) {
            progressDialog = ProgressDialog(activity)
            progressDialog!!.setMessage("Loading....")
        }

        progressDialog!!.show()
    }

    protected fun hideProgressDialog() {

        if (progressDialog != null) {
            progressDialog!!.dismiss()
        }
        progressDialog = null
    }

    protected fun showLongToast(content: String) {
        Toast.makeText(activity, content, Toast.LENGTH_LONG).show()
    }
}