package com.example.oleg.fuelbody.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.oleg.fuelbody.R
import com.example.oleg.fuelbody.models.FuelModel

/**
 * Created by Oleg on 11.06.2017.
 */
class FuelsAdapter(var fuels: List<FuelModel>) : RecyclerView.Adapter<FuelsAdapter.ViewHolder>() {
    private var view: View? = null
    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        view = LayoutInflater.from(parent?.context).inflate(R.layout.fuel_layot, parent, false)
        return ViewHolder(view!!)
    }

    override fun getItemCount(): Int {
        return fuels.size
    }

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        holder?.sumBoughtFuel?.setText(fuels[position].sumBoughtFuel)
        holder?.lastTimeBought?.setText(fuels[position].lastTimeBought)
        holder?.fuelName?.setText(fuels[position].fuelName)
        holder?.fuelAdress?.setText(fuels[position].fuelAdress)
        holder?.fuelDistance?.setText(fuels[position].fuelDistance)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val fuelLogo: ImageView = view.findViewById(R.id.fuel_station_logo) as ImageView
        val sumBoughtFuel: TextView = view.findViewById(R.id.last_fuel_sum) as TextView
        val lastTimeBought: TextView = view.findViewById(R.id.last_fuel_time) as TextView
        val fuelName: TextView = view.findViewById(R.id.fuel_name) as TextView
        val fuelAdress: TextView = view.findViewById(R.id.fuel_adress) as TextView
        val fuelDistance: TextView = view.findViewById(R.id.fuel_distance) as TextView
    }
}