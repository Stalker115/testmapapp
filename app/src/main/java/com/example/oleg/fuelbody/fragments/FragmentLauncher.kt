package com.example.oleg.fuelbody.fragments

import android.support.v4.app.FragmentManager
import android.text.TextUtils
import com.example.oleg.fuelbody.R

/**
 * Created by Oleg on 10.06.2017.
 */
class FragmentLauncher(manager: FragmentManager) {
    private val manager = manager

    private fun launch(fragment: GenericFragment, tag: String?) {
        val transaction = manager.beginTransaction()

        if (!TextUtils.isEmpty(tag)) {
            // transaction.addToBackStack(tag)
        }
        transaction.replace(R.id.fragment_container, fragment)
        transaction.commitAllowingStateLoss()
    }

    fun showMap() {
        val mapFragment = MapFragment()
        launch(mapFragment, MapFragment::class.java.simpleName)
    }
}