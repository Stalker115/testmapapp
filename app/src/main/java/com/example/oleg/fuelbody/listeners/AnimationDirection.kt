package com.example.oleg.fuelbody.listeners

/**
 * Created by Oleg on 12.06.2017.
 */
interface AnimationDirection {
    fun direction()
}