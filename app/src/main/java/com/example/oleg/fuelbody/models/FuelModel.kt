package com.example.oleg.fuelbody.models

/**
 * Created by Oleg on 11.06.2017.
 */
data class FuelModel(val logoImageLink: String, val sumBoughtFuel: String, val lastTimeBought:String,
                     val fuelName:String, val fuelAdress:String, val fuelDistance:String) {

}