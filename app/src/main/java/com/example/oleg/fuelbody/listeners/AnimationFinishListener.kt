package com.example.oleg.fuelbody.listeners

import android.animation.Animator
import android.util.Log

/**
 * Created by Oleg on 12.06.2017.
 */
class AnimationFinishListener(callback:AnimationDirection): Animator.AnimatorListener {
    private val TAG: String = AnimationFinishListener::class.java.simpleName
    private var call:AnimationDirection? = null
    init {
        call = callback
    }
    override fun onAnimationRepeat(animation: Animator?) {
        Log.d(TAG,"Animation repeat")
    }

    override fun onAnimationEnd(animation: Animator?) {
        Log.d(TAG,"Animation end")
    }

    override fun onAnimationStart(animation: Animator?) {
        Log.d(TAG,"Animation start")
        call?.direction()
    }

    override fun onAnimationCancel(animation: Animator?) {
        Log.d(TAG,"Animation cancel")
    }


}