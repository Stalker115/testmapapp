package com.example.oleg.fuelbody.fragments

import android.media.Image
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import com.example.oleg.fuelbody.R
import com.example.oleg.fuelbody.adapters.FuelsAdapter
import com.example.oleg.fuelbody.listeners.AnimationDirection
import com.example.oleg.fuelbody.listeners.AnimationFinishListener
import com.example.oleg.fuelbody.models.FuelModel
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.*
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import java.util.ArrayList

/**
 * Created by Oleg on 10.06.2017.
 */
class MapFragment : GenericFragment(), OnMapReadyCallback, AnimationDirection, View.OnClickListener {
    private val TAG = MapFragment::class.java.simpleName
    private var myView: View? = null
    private var gMap: GoogleMap? = null
    private var scrollButton: LinearLayout? = null
    private var recycler: RecyclerView? = null
    private var direction: Boolean? = false
    private var leftListArrow: ImageView? = null
    private var rightListArrow: ImageView? = null
    private var sortedByDistance: LinearLayout? = null
    private var sortedByCost: LinearLayout? = null
    private var recyclerContainer: LinearLayout? = null
    private val lat = LatLng(51.503186, -0.126446)
    private var mainMapView: SupportMapFragment? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        myView = inflater?.inflate(R.layout.fragment_map, container, false)
        val fm = childFragmentManager
        mainMapView = fm.findFragmentById(R.id.map_view) as? SupportMapFragment
        scrollButton = myView?.findViewById(R.id.anim_button_recycler) as? LinearLayout
        recycler = myView?.findViewById(R.id.main_recycler) as? RecyclerView
        sortedByDistance = myView?.findViewById(R.id.sorted_by_distance)as? LinearLayout
        sortedByCost = myView?.findViewById(R.id.sorted_by_cost) as? LinearLayout
        recyclerContainer = myView?.findViewById(R.id.list_map_fragment) as? LinearLayout
        leftListArrow = myView?.findViewById(R.id.left_arrow_list) as? ImageView
        rightListArrow = myView?.findViewById(R.id.right_arrow_list) as? ImageView

        sortedByDistance?.setOnClickListener(this)
        sortedByCost?.setOnClickListener(this)
        mainMapView?.getMapAsync(this)
        scrollButton?.setOnClickListener {
            if (direction!! == false) {
                recyclerContainer?.animate()?.translationY(300f)?.setListener(AnimationFinishListener(this))
                recyclerContainer?.animate()?.setDuration(2000)

            } else {
                recyclerContainer?.animate()?.translationY(0f)?.setListener(AnimationFinishListener(this))
                recyclerContainer?.animate()?.setDuration(2000)
            }

        }
        return myView
    }

    override fun onMapReady(p0: GoogleMap?) {
        gMap = p0
        gMap?.uiSettings?.isZoomGesturesEnabled
        gMap?.addMarker(MarkerOptions().position(lat).title("London is the capital of Great Britain"))
        gMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(lat, 10f))
        Log.d(TAG, "onCreateView: " + gMap)
    }

    override fun onResume() {
        super.onResume()
        recycler?.adapter = FuelsAdapter(createListFuels())
        recycler?.layoutManager = LinearLayoutManager(activity)
        mainMapView?.onResume()
    }

    override fun onDestroyView() {
        mainMapView?.onDestroy()
        super.onDestroyView()
    }

    private fun createListFuels(): List<FuelModel> {
        val fuelList = ArrayList<FuelModel>()
        val model1 = FuelModel("", "145", "час назад", "Okko", "ул. Молдавских Партизан 13", "25 km")
        fuelList.add(model1)
        fuelList.add(model1)
        fuelList.add(model1)
        fuelList.add(model1)
        fuelList.add(model1)
        fuelList.add(model1)
        fuelList.add(model1)
        fuelList.add(model1)
        fuelList.add(model1)
        fuelList.add(model1)
        fuelList.add(model1)
        fuelList.add(model1)
        return fuelList
    }

    override fun direction() {
        if (direction!!) {
            direction = false
        } else {
            direction = true
        }
    }

    override fun onClick(v: View?) {
       when(v?.id){
           R.id.sorted_by_distance -> consume { leftListArrow?.visibility = View.VISIBLE
                                                rightListArrow?.visibility = View.INVISIBLE
                                        Log.d(TAG, "onCreateView: delete1")}
           R.id.sorted_by_cost -> consume { leftListArrow?.visibility = View.INVISIBLE
                                            rightListArrow?.visibility = View.VISIBLE
                                        Log.d(TAG, "onCreateView: delete2")}
       }
    }

    inline fun consume(f: () -> Unit) {
         f()
    }

}